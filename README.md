# User management - frontend

This is frontend part of simple user management system using Laravel 5.3 and Angular 4. Backend can be found [here](https://gitlab.com/Zag/ji-148e0).

## Getting Started

Follow instructions below to prepare project for development.

### Prerequisites

To successfully use development version of this project you will need to install NodeJS and Angular CLI.

Please refer to [this](https://nodejs.org/en/) site for instructions on how to install NodeJS on your system.

After installing NodeJS install Angular CLI with:

```
npm install -g @angular/cli
```

### Installing

Clone repository and move to project root.

Install dependencies.

```
npm install
```

Angular CLI enables environment specific configuration. So that we do not have to mess with this built in feature we
generate environment files used by Angular CLI. So freshly cloned repository does not have real environment files yet.
We generate files with command below.

Note: That you have to run this command after each modification in files located in ```raw-environments```.

```
./prepare-configs.sh
```

Run development server.

```
ng serve -H 127.0.0.1 --proxy-config proxy.config.json
```

## Running the tests

Angular CLI actually prepares files for automated testing, but unfortunately this project does not have much tests yet
(we simply did not come that far yet :().

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository

## Authors

Inital content for this project was provided by Matic Zagmajster. For more information please see ```AUTHORS``` file.

## License

This project is licensed under the MIT License - see the ```LICENSE.md``` file for details.

