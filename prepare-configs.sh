#!/bin/bash

p="./raw-environments"

pf="./src/environments"

base="environment"

# Create env files.
rbase="$p/environment.ts"
rdev="$p/specific.ts"
rpro="$p/specific.prod.ts"

dev="$pf/$base.ts"
pro="$pf/$base.prod.ts"

`cat $rbase > $dev`
`cat $rbase > $pro`

`cat $rdev >> $dev`
`cat $rpro >> $pro`
