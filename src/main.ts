import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { AppConfig } from './environments/environment';


let config = new AppConfig();
if (config.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
