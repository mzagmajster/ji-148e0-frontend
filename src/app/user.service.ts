import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { NGXLogger } from 'ngx-logger';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var format = require('string-format');

import { AppConfig } from '../environments/environment';
import { User } from './user';
import { HttpErrorHandler } from './http-error-handler';
import { MessagingService } from './messaging.service';


@Injectable()
export class UserService extends HttpErrorHandler {

  private headers = new Headers({'Content-Type': false});

  constructor(private http: Http,
              private logger: NGXLogger,
              private config: AppConfig,
              private msgService: MessagingService) {
    super();
  }

  rUserData(uuid): Observable<User> {
    return this.http.get(this.config.urlFor('users.get', uuid))
      .map((response) => {
        let nu = response.json().obj as User;
        return new User(nu);
      })
      .catch(this.handleErrorObservable);
  }
  
  rUserList(dbField='uuid', orderDirection='asc', skip=0, take=10): Observable<User[]> {
    return this.http.get(
      this.config.urlFor('users.list', dbField, orderDirection, skip, take),
      {headers: this.headers}
    )
      .map(response => {
        let d = response.json();
        let ul: Array<User> = [];
        for(let i = 0; i < d.length; i++) {
          let r = new User(d[i] as User);
          ul.push(r);
        }
        return ul;
      })
      .catch(this.handleErrorObservable);
  }

  rUserRemoval(uuid): Observable<boolean> {
    return this.http.get(this.config.urlFor('users.remove', uuid), {headers: this.headers})
      .map(r => {
        let d = r.json();
        this.msgService.newMessages(d.messages);
        return d.success;
      })
      .catch(r => {
        let d = r.json();
        if(r.status == 403) {
          this.msgService.newMessages(d.messages);
        }
        else if(r.status == 404) {
          this.msgService.newMessage(format("User with ID: {0} was not found.", uuid), 'danger');
        }
        return this.handleErrorObservable(r);
      });
  }

  sUserData(uuid, view, values): Observable<boolean> {
    this.logger.debug('UserService#sUserData ', uuid, values);
    return this.http.post(this.config.urlFor('users.edit', uuid, view), values)
      .map(response => {
        let o = response.json();
        this.msgService.newMessages(o.messages);
        return o.success;
      })
      .catch(this.handleErrorObservable);
  }
  
  sNewUserData(values): Observable<boolean> {
    return this.http.post(this.config.urlFor('users.add'), values)
      .map(r => {
        let d = r.json();
        this.msgService.newMessages(d.messages);
        return d.success
      })
      .catch(this.handleErrorObservable)
  }

}
