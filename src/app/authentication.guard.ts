import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { NGXLogger } from 'ngx-logger';
import 'rxjs/add/operator/take';

import { AuthenticationService } from './authentication.service';


@Injectable()
export class AuthenticationGuard implements CanActivate {
  private isAuthenticated$: Observable<boolean>;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private logger: NGXLogger) {
    this.isAuthenticated$ = this.authService.isAuthenticated();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.logger.debug('AuthenticationGuard#CanActivate');

    return this.authService.getUser()
      .then(u => {
        if (!u.isAuthenticated()) {
          this.authService.redirectUrl = state.url;
          this.router.navigate(['/login']);
        }
        return u.isAuthenticated();
      })
      .catch((error) => {
        return false;
      });
  }
}
