import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthenticationService } from './authentication.service';
import { MessagingService } from './messaging.service';
import { Message } from './message';
import { User } from './user';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {

  isAuthenticated$: Observable<boolean>;
  currentUser$: Observable<User>;
  messages$: Observable<Message[]>;

  constructor(
      private authService: AuthenticationService,
      private msgService: MessagingService
  ) {
    this.isAuthenticated$ = this.authService.isAuthenticated();
    this.currentUser$ = this.authService.currentUser();
    this.messages$ = this.msgService.messages();
  }

  showProfileUrl(u: User): string {
    return '/users/show/' + u.uuid;
  }

  listUsersUrl(): string {
    return '/admin/users';
  }

  addNewUserUrl(): string {
    return '/admin/users/add'
  }

  ngOnInit() {}

  ngOnDestroy() {}
}
