import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { NGXLogger } from 'ngx-logger';

import { AuthenticationService } from './authentication.service';


@Injectable()
export class AnonymousGuard implements CanActivate {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private logger: NGXLogger) {
    
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.logger.debug('AnonymousGuard#canActivate');

    return this.authService.getUser()
      .then(u => {
        if (u.isAuthenticated()) {
          this.router.navigate([this.authService.getNextUrl()]);
        }
        return !u.isAuthenticated();
      })
      .catch(error => {
        return true;
      });
  }
}
