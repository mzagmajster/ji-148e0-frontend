import { Observable } from 'rxjs';


export class HttpErrorHandler {
  protected handleError(error:any): void {
    Promise.reject(error.message || error);
  }

  protected handleErrorObservable(error: Response | any) {
    return Observable.throw(error.message || error);
  }
}
