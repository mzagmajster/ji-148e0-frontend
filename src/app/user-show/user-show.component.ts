import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../authentication.service';
import { User } from '../user';


@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.css']
})
export class UserShowComponent implements OnInit {

  currentUser$: Observable<User>;

  constructor(private authService: AuthenticationService) {
    this.currentUser$ = this.authService.currentUser();
  }
  
  ngOnInit() {
  }

}
