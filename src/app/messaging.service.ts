import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { NGXLogger } from 'ngx-logger';

import { Message } from './message';


@Injectable()
export class MessagingService {
  private messagesSource = new Subject<Message[]>();

  constructor(
    private logger: NGXLogger) {
    this.logger.debug('Msg service constructor.');
  }

  newMessage(message, category) {
    let m = {message: message, category: category} as Message;
    this.messagesSource.next([m]);
  }

  newMessages(messages) {
    let fa: Message[] = [];
    for (let i = 0; i < messages.length; i++) {
      fa.push({message: messages[i][0], category: messages[i][1]} as Message);
    }
    this.messagesSource.next(fa);
  }

  messages(): Observable<Message[]> {
    return this.messagesSource.asObservable();
  }
}
