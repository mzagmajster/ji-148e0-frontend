import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { User } from '../user';
import { UserService } from '../user.service';
import { AuthenticationService } from '../authentication.service';
import {Subscription} from "rxjs/Rx";


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: [
    './users-list.component.css',
    '../../../node_modules/github-fork-ribbon-css/gh-fork-ribbon.css'
  ]
})
export class UsersListComponent implements OnInit {

  filters = {
    dbField: 'uuid',
    orderDirection: 'asc',
    skip: 0,
    take: 10
  };
  moreRecords: boolean;
  currentUser$: Observable<User>;
  userList: User[];

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.currentUser$ = this.authService.currentUser();
    this.moreRecords = false;
    this.userList = [];
    this.triggerRUserList()
  }

  userEditUrl(u: User) {
    return '/users/edit';
  }

  editProfile(u: User) {
    this.router.navigate(['/admin/users/edit', u.uuid]);
  }

  deleteProfile(u: User) {
    this.userService.rUserRemoval(u.uuid).subscribe(success => {
      if(success) {
        let index = this.userList.indexOf(u);

        if(index != -1) {
          // Removed user found on list.
          this.userList.splice(index, 1);
        }

      }
    });
  }

  triggerRUserList() {
    this.userService.rUserList(
      this.filters.dbField,
      this.filters.orderDirection,
      this.filters.skip,
      this.filters.take
    )
      .take(1)
      .subscribe(ul => {
        if(ul.length) {
          this.moreRecords = true;
        }
        else {
          this.moreRecords = false;
        }

        for(let i = 0; i < ul.length; i++) {
          this.userList.push(ul[i]);
        }
      });
  }

  loadNextRecords() {
    console.log('Load next');
    this.filters.skip += this.filters.take;
    this.filters.take = 10;
    this.triggerRUserList()
  }

  ngOnInit() {}

}
