import { Component, OnChanges, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { NgForm, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import 'rxjs/add/operator/take';

import { UserService } from '../user.service';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})

export class UserEditComponent implements OnChanges, OnInit, OnDestroy {

  currentUser$: Observable<User>;
  editUser$: Observable<User>;
  editUserId: number;
  
  userEditForm: FormGroup;
  formGroups: Array<string>;
  adminView: boolean = false;

  constructor(
    private authService: AuthenticationService,
    private userService: UserService,
    private logger: NGXLogger,
    private fb: FormBuilder,
    private el: ElementRef,
    private activatedRoute: ActivatedRoute) {
    this.currentUser$ = this.authService.currentUser();
    this.formGroups = [];
  }

  createForm(accountType: string = 'member') {
    let memberFields = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password2: '',
      birth_date: '',
      avatar: ''
    };

    let adminFields = {};

    if(accountType == 'admin') {
      adminFields = Object.assign(adminFields, {admin: '0'});
    }

    this.userEditForm = this.fb.group({
      memberFields: this.fb.group(memberFields),
      adminFields: this.fb.group(adminFields)
    });

    this.formGroups.push('memberFields');
    this.formGroups.push('adminFields');
  }

  onSubmit() {
    let inputE = this.el.nativeElement.querySelector('#avatar');
    this.logger.debug(this.userEditForm.value);
    let formData = new FormData();

    // Populate form data.
    for(let i = 0; i < this.formGroups.length; i++) {
      for(let k in this.userEditForm.get(this.formGroups[i]).value) {
        formData.append(k, this.userEditForm.get(this.formGroups[i]).get(k).value);
      }
    }

    if(inputE.files.length > 0) {
      formData.append('avatar', inputE.files.item(0));
    }
    else {
      formData.delete('avatar');
    }

    // Normalize some values (for server side validation process).
    let rawAdmin = formData.get('admin');
    this.logger.debug('rawAdmin ' + rawAdmin);

    let adminValue = (rawAdmin) ? '1' : '0';
    formData.set('admin', adminValue);

    let view = (this.adminView) ? 'admin' : 'member';
    this.userService.sUserData(this.editUserId, view, formData).take(1).subscribe(success => {

      this.currentUser$.take(1).subscribe(u => {
        // Current user was edited. Update client site object.
        if(u.uuid == this.editUserId) {
          this.authService.getUser();
        }
      });
    });
  }

  ngOnChanges() {}

  ngOnInit() {
    let accountType = 'member';
    this.activatedRoute.params.take(1).subscribe(p => {
      if('id' in p) {
        this.editUserId = p['id'];
        this.editUser$ = this.userService.rUserData(this.editUserId);
        accountType = 'admin';
        this.adminView = true;
      }
      else {
        this.currentUser$.take(1).subscribe((u) => {
          this.editUser$ = this.userService.rUserData(u.uuid);
          this.editUserId = u.uuid;
          this.adminView = false;
        });
      }
      this.createForm(accountType);
    });
  }

  ngOnDestroy() {}

}
