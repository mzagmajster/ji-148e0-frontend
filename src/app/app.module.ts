import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppRoutingModule } from './app-routing/app-routing.module';
import {AppLoggingModule} from "./app-logging/app-logging.module";
import { AuthenticationService } from './authentication.service';
import { MessagingService } from './messaging.service';
import { UserService } from './user.service';
import { AuthenticationGuard } from './authentication.guard';
import { AnonymousGuard } from './anonymous.guard';
import { AdminGuard } from './admin.guard';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppConfig } from '../environments/environment';
import { LogoutComponent } from './logout/logout.component';
import { MessageComponent } from './message/message.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserShowComponent } from './user-show/user-show.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserEditFieldsComponent } from './user-edit-fields/user-edit-fields.component';
import { UserEditFieldsAdminComponent } from './user-edit-fields-admin/user-edit-fields-admin.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    MessageComponent,
    UsersListComponent,
    UserAddComponent,
    UserEditComponent,
    UserShowComponent,
    UserDetailComponent,
    UserEditFieldsComponent,
    UserEditFieldsAdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CommonModule,
    InfiniteScrollModule,
    AppRoutingModule,
    AppLoggingModule
  ],
  providers: [
    AppConfig,
    AuthenticationService,
    UserService,
    AuthenticationGuard,
    AnonymousGuard,
    AdminGuard,
    MessagingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
