import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoggerModule, NGXLogger } from 'ngx-logger';

import { NgxConfig } from '../../environments/environment';


@NgModule({
  imports: [
    CommonModule,
    LoggerModule.forRoot(NgxConfig)
  ],
  exports: [],
  providers: []
})
export class AppLoggingModule { }
