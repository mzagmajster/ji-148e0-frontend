import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import 'rxjs/add/operator/takeWhile';


import { AuthenticationService } from '../authentication.service';
import { MessagingService } from '../messaging.service';
import { User } from '../user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private logger: NGXLogger,
    private msgService: MessagingService) {
    this.logger.debug('LC#Constructor');
  }

  onSubmit(f: NgForm) {
    this.authService.getToken().subscribe(token => {
      this.authService.login(f.value.email, f.value.password).subscribe(o => {
        if(o.isAuthenticated()) {
          this.router.navigate([this.authService.getNextUrl()]);
        }
      });
    });
  }

  ngOnInit() {
    this.logger.debug('LC#ngOnInit');
  }

  ngOnDestroy() {}

}
