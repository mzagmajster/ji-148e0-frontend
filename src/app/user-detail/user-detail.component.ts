import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input()
  detail: string = null;
  
  @Input()
  data: string = null;

  constructor() { }

  ngOnInit() {
  }

}
