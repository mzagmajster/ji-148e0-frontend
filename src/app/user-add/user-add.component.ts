import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';

import { User } from '../user';
import { UserService } from '../user.service';
import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})

export class UserAddComponent implements OnInit {

  currentUser$: Observable<User>;
  newUser: User;
  userAddForm: FormGroup;

  constructor(
    private userService: UserService,
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private router: Router,
    private el: ElementRef
  ) {
    this.currentUser$ = this.authService.currentUser();
    this.createForm();
  }

  createForm() {
    let memberFields = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password2: '',
      birth_date: '',
      avatar: ''
    };

    let adminFields = {admin: ''};

    this.userAddForm = this.fb.group({
      memberFields: this.fb.group(memberFields),
      adminFields: this.fb.group(adminFields)
    });
  }

  onSubmit() {
    let inputE = this.el.nativeElement.querySelector('#avatar');
    let formData = new FormData();

    let fg = ['memberFields', 'adminFields'];

    // Populate form data.
    for(let i = 0; i < fg.length; i++) {
      for(let k in this.userAddForm.get(fg[i]).value) {
        formData.append(k, this.userAddForm.get(fg[i]).get(k).value);
      }
    }

    if(inputE.files.length > 0) {
      formData.append('avatar', inputE.files.item(0));
    }
    else {
      formData.delete('avatar');
    }

    // Normalize some values (for server side validation process).
    let rawAdmin = formData.get('admin');
    let adminValue = (rawAdmin) ? '1' : '0';
    formData.set('admin', adminValue);

    this.userService.sNewUserData(formData).take(1).subscribe(success => {
      if(success) {
        this.router.navigate(['/admin/users']);
      }
    });
  }

  ngOnInit() {
    this.newUser = new User();
  }

}
