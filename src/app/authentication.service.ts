import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { NGXLogger } from 'ngx-logger';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User } from './user';
import { HttpErrorHandler } from './http-error-handler';
import { AppConfig } from '../environments/environment';
import { MessagingService } from './messaging.service';


@Injectable()
export class AuthenticationService extends HttpErrorHandler{

  //  General stuff.
  private headers = new Headers({'Content-Type': 'application/json'});
  private csrfToken = null;
  public redirectUrl: string = '';

  // Current user.
  private currentUserSource = new BehaviorSubject<User>(new User());
  private isAuthenticatedSource = new BehaviorSubject<boolean>(false);

  constructor(
    private http: Http,
    private config: AppConfig,
    private msgService: MessagingService,
    private logger: NGXLogger) {

    super();
    this.logger.debug('Auth service constructor.');
  }

  private setCurrentUser(o: User) {
    let loggedIn = (o.uuid != -1) ? true : false;
    this.logger.debug('loggedIn ' + loggedIn);

    this.isAuthenticatedSource.next(loggedIn);
    this.currentUserSource.next(o);
  }

  getUser(): Promise<User> {
    return this.http.get(this.config.urlFor('isLoggedIn'))
      .toPromise()
      .then(response => {
        let o = response.json().obj as User;
        let ro = new User(o);
        this.logger.debug('currentUserId ' + ro.uuid);
        this.setCurrentUser(ro);
        return ro;
      })
      .catch(this.handleError);
  }

  getToken(): Observable<string> {
    return this.http.get(this.config.urlFor('getToken'))
      .map(response => {
        let r = response.json();
        this.csrfToken = r._token;
        return this.csrfToken;
      })
      .catch(this.handleErrorObservable);
  }

  login(email, password): Observable<User> {
    return this.http.post(
      this.config.urlFor('login'),
      JSON.stringify({email: email, password: password, _token: this.csrfToken}),
      {headers: this.headers}
    )
      .map(response => {
        let jo = response.json();
        let o = new User(jo.obj);
        this.setCurrentUser(o);
        this.msgService.newMessages(jo.messages);
        return o;
      })
      .catch((response) => {
        let e = response.json();
        // Validation error.
        if(e.status = 422) {
          this.logger.debug(e);
          let d = [];
          for(let i = 0; i < e.email.length; i++) {
            d.push([e.email[i], 'danger']);
          }
          for(let i = 0; i < e.password.length; i++) {
            d.push([e.password[i], 'danger'])
          }
          this.msgService.newMessages(d);
        }
        return this.handleErrorObservable(e);
      });
  }

  logout(): Promise<User> {
    return this.http.get(this.config.urlFor('logout'))
      .toPromise()
      .then(response => {
        let jo = response.json();
        let o = new User(jo.obj);
        this.setCurrentUser(o);
        this.msgService.newMessages(jo.messages);
        return o;
      })
      .catch(this.handleError);
  }

  currentUser(): Observable<User> {
    return this.currentUserSource.asObservable().share();
  }

  isAuthenticated(): Observable<boolean> {
    return this.isAuthenticatedSource.asObservable().share();
  }

  getNextUrl(): string {
    let url = '/users/edit';
    
    if (this.redirectUrl.length) {
      url = this.redirectUrl;
      this.redirectUrl = '';
    }
    
    return url;
  }

}
