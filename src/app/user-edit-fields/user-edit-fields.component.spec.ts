import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditFieldsComponent } from './user-edit-fields.component';

describe('UserEditFieldsComponent', () => {
  let component: UserEditFieldsComponent;
  let fixture: ComponentFixture<UserEditFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEditFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
