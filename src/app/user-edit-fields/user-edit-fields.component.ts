import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { User } from '../user';


@Component({
  selector: 'user-edit-fields',
  templateUrl: './user-edit-fields.component.html',
  styleUrls: ['./user-edit-fields.component.css']
})

export class UserEditFieldsComponent implements OnInit, OnChanges {

  @Input()
  user: User;
  
  @Input()
  userEditForm: FormGroup;
  
  constructor() { }

  ngOnInit() {}
  
  ngOnChanges() {
    // TODO: Fix: Split regular fields and admin fields into different groups.
    this.userEditForm.get('memberFields').reset({
      first_name: this.user.first_name || '',
      last_name: this.user.last_name || '',
      email: this.user.email || '',
      password: '',
      password2: '',
      birth_date: this.user.birth_date,
      avatar: ''
    });
  }

}
