import { AppConfig } from '../environments/environment';


var config = new AppConfig();

export class User {
  // DB fields.
  uuid: number;
  first_name: string;
  last_name: string;
  birth_date: Date;
  avatar: string;
  email: string;
  created: Date;
  admin: number;
  
  // Derived fields.
  status: string;
  
  constructor(src?: User) {
    this.uuid           = src && src.uuid || -1;
    this.first_name     = src && src.first_name || null;
    this.last_name      = src && src.last_name || null;
    this.birth_date     = src && src.birth_date || null;
    this.avatar         = src && src.avatar || null;
    this.email          = src && src.email || null;
    this.created        = src && src.created || null;
    this.admin          = src && src.admin || 0;
    this.status         = src && src.status || "Guest";
  }
  
  /**
   * If user is not authenticated it is anonymous.
   *
   * @returns {boolean}
   */
  isAuthenticated(): boolean {
    return (this.uuid != -1) ? true : false;
  }

  isAdmin(): boolean {
    return (this.admin) ? true : false;
  }

  /**
   * Get relevant avatar.
   * 
   * @returns {string} If avatar exists for given user object absolute path to avatar, relative path to default 
   * avatar otherwise.
   */
  getAvatar(): string {
    return (this.avatar) ? (config.backendBaseUrl + this.avatar) : '/assets/default-avatar.jpg';
  }
}
