import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEditFieldsAdminComponent } from './user-edit-fields-admin.component';

describe('UserEditFieldsAdminComponent', () => {
  let component: UserEditFieldsAdminComponent;
  let fixture: ComponentFixture<UserEditFieldsAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserEditFieldsAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditFieldsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
