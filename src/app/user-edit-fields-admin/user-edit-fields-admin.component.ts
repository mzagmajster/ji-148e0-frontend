import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { User } from '../user';


@Component({
  selector: 'user-edit-fields-admin',
  templateUrl: './user-edit-fields-admin.component.html',
  styleUrls: ['./user-edit-fields-admin.component.css']
})

export class UserEditFieldsAdminComponent implements OnInit, OnChanges {
  @Input()
  user: User;
  
  @Input()
  userEditForm: FormGroup;
  
  constructor() { }

  ngOnInit() {}
  
  ngOnChanges() {
    this.userEditForm.get('adminFields').reset({
      admin: this.user.isAdmin() || ''
    });
  }

}
