import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';

import { AuthenticationService } from './authentication.service';
import { User } from './user';


@Injectable()
export class AdminGuard implements CanActivate {

  currentUser$: Observable<User>;

  constructor(private authService: AuthenticationService) {
    this.currentUser$ = this.authService.currentUser();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.currentUser$.map(u => {
      return u.isAdmin();
    });
  }
}
