import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationGuard } from '../authentication.guard';
import { AnonymousGuard } from '../anonymous.guard';
import { AdminGuard } from '../admin.guard';
import { LoginComponent } from '../login/login.component';
import { LogoutComponent } from '../logout/logout.component';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserShowComponent } from '../user-show/user-show.component';
import { UsersListComponent } from '../users-list/users-list.component';


const routes: Routes = [
  {
    path: 'login',
    canActivate: [ AnonymousGuard ],
    component: LoginComponent,
    data: {
      routeId: 'login'
    }
  },
  {
    path: 'logout',
    canActivate: [ AuthenticationGuard ],
    component: LogoutComponent,
    data: {
      routeId: 'logout'
    }
  },
  {
    path: 'admin',
    canActivate: [ AuthenticationGuard, AdminGuard ],
    children: [
      {
        path: 'users',
        children: [
          {
            path: 'add',
            component: UserAddComponent
          },
          {
            path: 'edit/:id',
            component: UserEditComponent
          },
          {
            path: '',
            component: UsersListComponent
          }
        ],
        data: {
          routeId: 'users'
        }
      }
    ],
    data: {
      routeId: 'admin'
    }
  },
  {
    path: 'users',
    canActivate: [ AuthenticationGuard ],
    children: [
      {
        path: 'show/:id',
        component: UserShowComponent,
        data: {
          routeId: 'show'
        }
      },
      {
        path: 'edit',
        component: UserEditComponent,
        data: {
          routeId: 'edit'
        }
      }
    ],
    data: {
      routeId: 'users'
    }
  },
  {
    path: '',
    redirectTo: '/users/edit',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
