import { UserManagementFrontendPage } from './app.po';

describe('user-management-frontend App', () => {
  let page: UserManagementFrontendPage;

  beforeEach(() => {
    page = new UserManagementFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
