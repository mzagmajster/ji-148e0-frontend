// File: raw-environments/environments
import { Injectable } from '@angular/core';
import { NgxLoggerLevel } from 'ngx-logger';

var format = require('string-format');


export interface ApiRoute<T> {
  [routeId: string]: string;
}

// Main configuration
// Common & development config.
export class AppDefaultConfig {
  // Basic
  production: boolean = false;

  backendBaseUrl: string = 'http://127.0.0.1:8000';

  frontendBaseUrl: string = 'http://127.0.0.1:4200';

  // API
  apiRoutes: ApiRoute<string> = {
    // Authentication endpoints

    // GET
    'logout': '/api/auth/logout',
    'isLoggedIn': '/api/auth/is-authenticated',
    'getToken': '/api/temp/csrf',

    // POST
    // R: [email, password]
    'login': '/api/auth/login',

    // Users endpoints

    // GET
    // R: [uuid]
    'users.get': '/api/users/get/{0}',
    // R: [<database field>, <asc,desc> <number>, <number>]
    'users.list': '/api/users/?order-by={0}&order-direction={1}&skip={2}&take={3}',
    // R: [uuid]
    'users.remove': '/api/users/remove/{0}',

    // POST
    // R: [<User object>]
    'users.edit': '/api/users/edit/{0}?view={1}',
    // R: [<User object>]
    'users.add': '/api/users/add'
  };

  urlFor(routeId: string, ...params) : string {
      return format(this.apiRoutes[routeId], ...params);
  }

}

// Typical production config.
export class AppProductionConfig extends AppDefaultConfig {
  production = true;
  loggerLevel = NgxLoggerLevel.ERROR;
}

// NgxLogger config.
const NgxDefaultConfig = {
  level: NgxLoggerLevel.DEBUG,
  serverLoggingUrl: '',
  serverLogLevel: NgxLoggerLevel.OFF
};

const NgxProductionConfig = {
  level: NgxLoggerLevel.ERROR,
  serverLoggingUrl: '',
  serverLogLevel: NgxLoggerLevel.OFF
};

// end: raw-environments/environments
